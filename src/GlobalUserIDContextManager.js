import React, { useState } from 'react'

export const GlobalUserIDContext= React.createContext({
    globalUserID: "",
    setGlobalUserIDContext: () => {}
})

export const GlobalUserIDContextProvider = (props) => {

    const setGlobalUserIDContext = (value) => {
        setState({...state, globalUserID: value})
    }

    const initState = {
        globalUserID: false,
        setGlobalUserIDContext: setGlobalUserIDContext
    }

    const [state, setState] = useState(initState)

    return (
        <GlobalUserIDContext.Provider value={state}>
            {props.children}
        </GlobalUserIDContext.Provider>
    )
}