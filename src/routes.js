
import ObjectUploadForm from "views/ObjectUploadForm/ObjectUploadForm.js";
import ObjectRetrievalForm from "views/ObjectRetrievalForm/ObjectRetrievalForm.js";
import LoginPage from "views/AdminScreens/LoginPage/LoginPage.js";
import RegisterPage from "views/AdminScreens/RegisterPage/RegisterPage.js";
import Search from "views/ObjectSearch/Search";
import CreateNewDomainPage from "./views/AdminScreens/CreateNewDomainPage/CreateNewDomainPage";
import CreateVolume from "./views/AdminScreens/CreateVolume/CreateVolume";
import NewApplication from "./views/AdminScreens/NewApplication/NewApplication";
import UpdateDomain from "./views/AdminScreens/UpdateDomain/UpdateDomain";
import AddVolumeToDomain from "./views/AdminScreens/AddVolumeToDomain/AddVolumeToDomain";
import AddUserToApplication from "./views/AdminScreens/AddUserToApplication/AddUserToApplication";


var dashRoutes = [
  {
    path: "/upload-object",
    name: "Upload Object",
    component: ObjectUploadForm,
    layout: "/user",
    adminOnly: false
  },
  {
    path: "/retrieve-object",
    name: "Retrieve Object",
    component: ObjectRetrievalForm,
    layout: "/user",
    adminOnly: false
  },
  {
    path: "/login",
    name: "Login",
    component: LoginPage,
    layout: "/auth",
    adminOnly: false
  },
  {
    path: "/search",
    name: "Search",
    component: Search,
    layout: "/user",
    adminOnly: false
  },
  {
    path: "/createDomain",
    name: "Create Domain",
    component: CreateNewDomainPage,
    layout: "/user",
    adminOnly: true
  },

  {
    path: "/newVolume",
    name: "New Volume",
    component: CreateVolume,
    layout: "/user",
    adminOnly: true
  },
  {
    path: "/newApp",
    name: "New App",
    component: NewApplication,
    layout: "/user",
    adminOnly: true
  },
  {
    path: "/changeDomain",
    name: "Update Domain",
    component: UpdateDomain,
    layout: "/user",
    adminOnly: true
  },
  {
    path: "/addVolumeToDomain",
    name: "Add Volume To Domain",
    component: AddVolumeToDomain,
    layout: "/user",
    adminOnly: true
  },{
    path: "/addUserToDomain",
    name: "Register",
    component: RegisterPage,
    layout: "/user",
    adminOnly: true
  },
  {
    path: "/addUserToApplication",
    name: "Add User To Application",
    component: AddUserToApplication,
    layout: "/user",
    adminOnly: true
  },
];

export default dashRoutes;
