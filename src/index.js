/*!

=========================================================
* Material Dashboard PRO React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import UserLayout from "layouts/User.js";
import AuthLayout from "layouts/Auth.js";
import {GlobalAPPIDContextProvider} from "./GlobalAPPIDContextManager"
import {GlobalBDIDContextProvider} from "./GlobalBDIDContextManager";

import "assets/scss/material-dashboard-pro-react.scss?v=1.9.0";
import {GlobalAdminContextProvider} from "./GlobalAdminContextManager";
import {GlobalUserIDContextProvider} from "./GlobalUserIDContextManager";

export const history  = createBrowserHistory();

ReactDOM.render(
    <GlobalUserIDContextProvider value={""}>
    <GlobalAdminContextProvider value={false}>
    <GlobalBDIDContextProvider value ={""}>
    <GlobalAPPIDContextProvider value={""}>
  <Router history={history}>
    <Switch>
        <Route path="/user" component={UserLayout} />
        <Route path="/auth" component={AuthLayout} />
      <Redirect from="/" to="/auth/login" />
    </Switch>
  </Router>
    </GlobalAPPIDContextProvider>
    </GlobalBDIDContextProvider>
    </GlobalAdminContextProvider>
    </GlobalUserIDContextProvider>, document.getElementById("root")

);
