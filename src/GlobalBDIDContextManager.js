import React, { useState } from 'react'

export const GlobalBDIDContext = React.createContext({
    globalBDID: "",
    setGlobalBDIDContext: () => {}
})

export const GlobalBDIDContextProvider = (props) => {

    const setGlobalBDIDContext = (value) => {
        setState({...state, globalBDID: value})
    }

    const initState = {
        globalBDID: "",
        setGlobalBDIDContext: setGlobalBDIDContext
    }

    const [state, setState] = useState(initState)

    return (
        <GlobalBDIDContext.Provider value={state}>
            {props.children}
        </GlobalBDIDContext.Provider>
    )
}