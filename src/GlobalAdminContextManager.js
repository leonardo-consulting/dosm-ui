import React, { useState } from 'react'

export const GlobalAdminContext= React.createContext({
    globalAdmin: false,
    setGlobalAdminContext: () => {}
})

export const GlobalAdminContextProvider = (props) => {

    const setGlobalAdminContext = (value) => {
        setState({...state, globalAdmin: value})
    }

    const initState = {
        globalAdmin: false,
        setGlobalAdminContext: setGlobalAdminContext
    }

    const [state, setState] = useState(initState)

    return (
        <GlobalAdminContext.Provider value={state}>
            {props.children}
        </GlobalAdminContext.Provider>
    )
}