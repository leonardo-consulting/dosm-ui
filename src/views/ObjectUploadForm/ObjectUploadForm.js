/*eslint-disable*/
import React, {useContext, useEffect} from "react";
import axios from 'axios';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// material ui icons
import Close from "@material-ui/icons/Close";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";


// style for this view
import styles from "assets/jss/material-dashboard-pro-react/views/validationFormsStyle.js";

import Modal from "views/UI/Modal/Modal.js";
import ModalMessage from "views/UI/Modal/UploadModal/modalMessage/modalMessage.js";
import ModalPreview from "views/UI/Modal/PreviewModal/modalPreview.js";

import ImageEditor from "views/ObjectUploadForm/ImageEditor.js"
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

import 'views/ObjectUploadForm/ImageEditor.scss'
import {GlobalAPPIDContext} from "../../GlobalAPPIDContextManager";
import {GlobalBDIDContext} from "../../GlobalBDIDContextManager";
import {GlobalUserIDContext} from "../../GlobalUserIDContextManager";

const useStyles = makeStyles(styles);

export default function ValidationForms() {
    // register form

    const [formData, setFormData] = React.useState([]);
    const [registerFiles, setregisterFiles] = React.useState([]);
    const [uploadModalActive, setuploadModalActive] = React.useState(false);
    const [previewModalActive, setpreviewModalActive] = React.useState(false);
    const [modalTitle, setmodalTitle] = React.useState("");
    const [modalMessage, setmodalMessage] = React.useState("");
    const appidState = useContext(GlobalAPPIDContext)
    const bdidState = useContext(GlobalBDIDContext)
    const userIDState = useContext(GlobalUserIDContext)


    // function that verifies if a string has a given length or not
    const verifyLength = (value, length) => {
        if (value.length <= length) {
            return true;
        }
        return false;
    };

    const verifyDate = (value) => {
        if (value.length != 10) return false;
        var dateParts = value.split("/");
        var date = new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
        if (date.getDate() == dateParts[0] && date.getMonth() == (dateParts[1] - 1) && date.getFullYear() == dateParts[2]) {
            return true;
        }
        else return false;
    }

    const setFields = (data) => {
        setFormData([]);
        for (let i = 0; i < data.length; i++) {
            const newField = {
                name: data[i]["fieldName"],
                length: data[i]["fieldLength"],
                type: data[i]["fieldType"],
                state: "",
                value: "",
            }
            setFormData(formData => [...formData, newField])
        }
    }


    useEffect(() => {
        axios.get("http://localhost:8080/getfields?appid=" + appidState.globalAPPID)
            .then((response) => {
                console.log(response)
                if (response.status == 200) {
                    setFields(response.data)
                } else {alert("error")
                }
            }, (error) => {
                alert("error")
            });

    }, [appidState.globalAPPID]);

    const setValueField = (name, value) => {

        const data = [...formData];
        const index = data.findIndex(m => m.name === name);
        data[index].value = value;

        setFormData(data)
    }

    const setStateField = (name, value) => {
        const data = [...formData];
        const index = data.findIndex(m => m.name === name);
        data[index].state = value;

        setFormData(data)
    }

    const renderForm = formData.map((field) => {
        return(
            <CustomInput
                key={field.name}
                success={field.state=== "success"}
                error={field.state === "error"}
                labelText={field["name"]}
                id={field["name"]}
                formControlProps={{
                    fullWidth: true
                }}
                inputProps={{
                    onChange: event => {
                        if (verifyLength(event.target.value, field.length) && field.type !== '2') {
                            setStateField(field.name, "success");
                        } else if (field.type === '2' && verifyDate(event.target.value)) {
                            setStateField(field.name, "success");
                        }
                        else {
                            setStateField(field.name, "error");
                        }
                        setValueField(field.name, event.target.value)
                    },
                    type: "keyword",
                    autoComplete: "off"
                }}
            />
        )})

    const onFileChange = event => {
        for (let i = 0; i < event.target.files.length; i++) {
            let file = event.target.files[i]
            setregisterFiles(registerFiles => [...registerFiles, file])
        }
    };

    const hiddenFileInput = React.useRef(null);

    const handleClick = () => {
        hiddenFileInput.current.click();
    };

    const onCancel = () => {
        setpreviewModalActive(false);
    }

    const onContinue = () => {
        setpreviewModalActive(false);
        sendData();
    }

    const onClose = () => {
        setuploadModalActive(false);
        //setregisterFiles([]);
        //clearUserInput()
    }

    const clearUserInput = () => {
        for (let i = 0; i < formData.length; i++) {
            setStateField(formData[i].name, "");
            setStateField(formData[i].name, "");
        }
    }


    const registerClick = () => {
        if (registerFiles.length === 0){
            alert("Please choose a file to continue");
        }
        else {
            setpreviewModalActive(true);
        }
    };

    const sendData = () => {
        const data = new FormData()
        for (let i = 0; i < formData.length; i++) {
            data.append(formData[i].name, formData[i].value)
        }
        data.append("globalbdid", bdidState.globalBDID)
        data.append("globalappid", appidState.globalAPPID)
        data.append("globaluserid", userIDState.globalUserID)

        data.append('file', registerFiles[0])

        axios.post("http://localhost:8080/putObject"
            , data)
            .then((response) => {
                console.log(response)
                if (response.status == 200) {

                    setmodalTitle("Success!")
                    setmodalMessage("Your object was uploaded successfully!")
                    setuploadModalActive(true)
                } else {
                    setmodalTitle("Uh oh!")
                    setmodalMessage("An error occured, please try again")
                    setuploadModalActive(true)
                }
            }, (error) => {
                console.log("Error")
                setmodalTitle("Uh oh!")
                setmodalMessage("An error occured, please try again")
                setuploadModalActive(true)
                console.log(error);
            });
    }

    const onFileDelete = (file) => {

        return( setregisterFiles(registerFiles.filter((item) => {
            return (item.name != file.name)
        })))
    };

    const renderFileNames = registerFiles.map((file) => {
        return(<div key={file.name} className={"ColumnDiv"}>
            <div className={"CenterDiv"}>
                <ImageEditor name={file.name}/>
                <IconButton color="primary" aria-label="upload picture" component="span" onClick={() => onFileDelete(file)} >
                    <CloseIcon />
                </IconButton>
            </div>
        </div>)})


    const classes = useStyles();
    return (
        <div >
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={6}>
                    <Card>
                        <h4 className={classes.cardIconTitle}>Upload Object</h4>
                        <CardBody>
                            <Modal show={uploadModalActive}>
                                < ModalMessage title={modalTitle} message={modalMessage} buttonMessage={"Close"} closed={onClose}/>
                            </Modal>
                            <Modal show={previewModalActive} >
                                < ModalPreview files={registerFiles} uploadCancelled={onCancel} uploadContinued={onContinue}/>
                            </Modal>
                            <form>
                                {renderForm}
                                <input type="file"
                                       multiple
                                       onChange={onFileChange}
                                       style={{display:'none'}}
                                       ref={hiddenFileInput}
                                />
                                <Button
                                    onClick={handleClick}
                                    className={classes.middleButton}
                                >
                                    Choose A File

                                </Button>
                                <Button
                                    color="rose"
                                    onClick={registerClick}
                                    className={classes.registerButton}
                                >
                                    Upload Object
                                </Button>
                            </form>
                            {renderFileNames}
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}