import React from "react";

import './ImageEditor.scss'

export default function ImageEditor(name) {

    const formatName = () => {
        let string = JSON.stringify(name)
        let formattedName = string.slice(9, string.length - 2)
        return (formattedName)
    }



    return (
                <small className="CenterText"> {formatName()}  </small>
        )
}

