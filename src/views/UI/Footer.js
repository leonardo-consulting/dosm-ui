/*eslint-disable*/
import React, {useCallback, useContext} from "react";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import cx from "classnames";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

import styles from "assets/jss/material-dashboard-pro-react/components/footerStyle.js";
import Button from "views/UI/Modal/Button/Button"
import "views/ObjectUploadForm/ImageEditor.scss"
import Modal from "views/UI/Modal/Modal";
import LogoutModal from "./Modal/LogoutModal/LogoutModal";
import axios from "axios";
import DropdownMenu from "./DropdownMenu/DropdownMenu";
import {GlobalAPPIDContext} from "../../GlobalAPPIDContextManager"
import {GlobalBDIDContext} from "../../GlobalBDIDContextManager";


const useStyles = makeStyles(styles);

export default function Footer(props) {
    const [modalShow, setmodalShow] = React.useState(false)
    const [redirect, setredirect] = React.useState(false)

    const classes = useStyles();
    const { fluid, white, rtlActive } = props;
    var container = cx({
        [classes.container]: !fluid,
        [classes.containerFluid]: fluid,
        [classes.whiteColor]: white
    });

    const clicked = () => {
        setmodalShow(true);
    }

    const logoutCancelled = () => {
        setmodalShow(false)
    }

    const logoutContinued = () => {
        setredirect(true)
    }

    const renderRedirect = () => {
        if (redirect) {
            return (
               <Redirect to={"/"}/>
            )
        }
    }

    const [applicationIds, setApplicationIds] = React.useState([]);
    const [applicationNames, setApplicationNames] = React.useState([]);
    const [renderApplicationDropdown, setRenderApplicationDropdown] = React.useState(false);
    const [selectedApplication, setSelectedApplication] =  React.useState("")

    const appidState = useContext(GlobalAPPIDContext)
    const bdidState = useContext(GlobalBDIDContext)
    //console.log(things)

    const wrapperSetSelectedApplication = useCallback(val => {
        setSelectedApplication(val)
    }, [setSelectedApplication])

    React.useEffect( () => {
        appidState.setGlobalAPPIDContext(selectedApplication)
    }, [selectedApplication])

    React.useEffect(() => {
        axios.post("http://localhost:8080/getAllApplications", {
            "globalbdid": bdidState.globalBDID
        }).then((response) => {
            const dataArray = Object.values(response.data);
            let ids = []
            let names = []
            for (let i = 0; i < dataArray.length; i++) {
                ids.push(dataArray[i]["globalappid"])
                names.push(dataArray[i]["appdescription"])
            }
            setApplicationIds(ids)
            setApplicationNames(names)
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
        setRenderApplicationDropdown(true);
        }, []);

    const renderApplicationMenu = () => {
        if (renderApplicationDropdown) {
            return (
                <DropdownMenu name={"Application"} names={applicationNames} ids={applicationIds} parentStateSetter={wrapperSetSelectedApplication}/>
            )
        }
    }

    return (
        <div>
            {renderRedirect()}
            <Modal show={modalShow}>
                <LogoutModal logoutCancelled={logoutCancelled} logoutContinued={logoutContinued} />
            </Modal>
        <footer className={classes.footer}>
            <div className={container}>
                <p className={classes.right}>
                    <Button clicked={clicked}> LOGOUT </Button>
                </p>
                {renderApplicationMenu()}
            </div>
        </footer>
        </div>
    );
}

Footer.propTypes = {
    fluid: PropTypes.bool,
    white: PropTypes.bool,
    rtlActive: PropTypes.bool
};
