import React, {useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import "./DropdownMenu.scss"

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function DropdownMenu(props) {
    const classes = useStyles();
    const [selected, setSelected] = React.useState('');
    const [values, setValues] = React.useState([]);

    React.useEffect(() => {
        if (props.names && props.ids) {
        for (let i = 0; i < props.names.length; i++) {
            let elem = {
                name: props.names[i],
                id: props.ids[i]
            }
            setValues(values => [...values, elem])
        }}
    }, [props.names, props.ids])

    const handleChange = (event) => {
        setSelected(event.target.value);
    };

    useEffect(() => {
        props.parentStateSetter(selected);
    }, [props.parentStateSetter, selected, props]);

    const mapValues = values.map((v) => {
        return (<MenuItem key={v.id} value={v.id}>{v.name}</MenuItem>)
    })

    return (
        <div className={"dropDown"}>
            <FormControl className={classes.formControl}>
                <InputLabel id="demo-simple-select-label">{props.name} </InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selected}
                    onChange={handleChange}
                >
                    {mapValues}
                </Select>
            </FormControl>
        </div>
    );
}