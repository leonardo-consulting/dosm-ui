import React from 'react';

import Button from "views/UI/Modal/Button/Button.js";
import "views/UI/Modal/PreviewModal/ButtonSplit.scss";

export default function LogoutModal(props) {

    return (<div
        style={{textAlign: 'center', alignSelf: 'stretch'}}>
        <h4> Are you sure you want to logout? </h4>
        <div >
            <div className={"button"}><Button classname={"cancel"} clicked={props.logoutCancelled}>
                Cancel
            </Button>
                <Button classname={"continue"} clicked={props.logoutContinued}>
                    Continue
                </Button> </div>
        </div>
    </div>)
}
