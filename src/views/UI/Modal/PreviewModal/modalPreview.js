import React from 'react';

import Button from "views/UI/Modal/Button/Button.js";
import './ImagePreview.scss';
import './ButtonSplit.scss';

export default function modalPreview(props) {

     const renderFiles = props.files.map((file) => {
        return (<img  key={file.name} className="ImagePreview" src={URL.createObjectURL(file)} alt={""}/>)
    })

    return (<div
        style={{textAlign: 'center', alignSelf: 'stretch'}}>
        <h4> You have uploaded the following files: </h4>
       <div >
            {renderFiles}
           <div className={"button"}><Button classname={"cancel"} clicked={props.uploadCancelled}>
                Cancel
            </Button>
            <Button classname={"continue"} clicked={props.uploadContinued}>
            Continue
        </Button> </div>
       </div>
    </div>)
}
