import React from 'react';

import Button from "views/UI/Modal/Button/Button.js";

const modalMessage = (props) => (
    <div
        style={{textAlign: 'center', alignSelf: 'stretch'}}>
        <h2> {props.title} </h2>
        <h4> {props.message}</h4>
        <Button className={"closeButton"} clicked={props.closed}>
            {props.buttonMessage}
        </Button>
    </div>
)

export default modalMessage;