/*eslint-disable*/
import React from "react";
import axios from 'axios';
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// material ui icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";

// style for this view
import styles from "assets/jss/material-dashboard-pro-react/views/validationFormsStyle.js";

const useStyles = makeStyles(styles);

export default function ValidationForms() {
    // register form
    const [registerObjectId, setregisterObjectId] = React.useState("");
    const [registerObjectIdState, setregisterObjectIdState] = React.useState("");

    // function that verifies if a string has a given length or not
    const verifyLength = (value, length) => {
        if (value.length >= length) {
            return true;
        }
        return false;
    };

    const registerClick = () => {
        if (registerObjectIdState === "" || registerObjectIdState === "error") {
            setregisterObjectIdState ("error");
        }
        else {
            axios({
                url: 'http://localhost:8080/getobject?objectid=' + registerObjectId,
                method: 'GET',
                responseType: 'blob',
            }).then((response) => {
                const fileName = response.headers['content-disposition'].split('filename=')[1];
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', fileName.slice(1, fileName.length - 1));
                document.body.appendChild(link);
                link.click();
            });
        }
    };

    const classes = useStyles();
    return (
        <GridContainer justify="center">
            <GridItem xs={12} sm={12} md={6}>
                <Card>
                    <CardHeader color="rose" icon>
                        <h4 className={classes.cardIconTitle}>Retrieve Object</h4>
                    </CardHeader>
                    <CardBody>
                        <form>
                            <CustomInput
                                success={registerObjectIdState === "success"}
                                error={registerObjectIdState === "error"}
                                labelText="Object Id *"
                                id="registerObjectId"
                                formControlProps={{
                                    fullWidth: true
                                }}
                                inputProps={{
                                    onChange: event => {
                                        if (verifyLength(event.target.value, 1)) {
                                            setregisterObjectIdState("success");
                                        } else {
                                            setregisterObjectIdState("error");
                                        }
                                        setregisterObjectId(event.target.value);
                                    },
                                    type: "objectId",
                                    autoComplete: "off"
                                }}
                            />
                            <div className={classes.formCategory}>
                                <small>*</small> Required fields
                            </div>
                            <Button
                                color="rose"
                                onClick={registerClick}
                                className={classes.registerButton}
                            >
                                Get Object
                            </Button>
                        </form>
                    </CardBody>
                </Card>
            </GridItem>
        </GridContainer>
    );
}
