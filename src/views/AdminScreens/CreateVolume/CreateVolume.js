import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import axios from "axios";

import Modal from "views/UI/Modal/Modal";
import ModalMessage from "../../UI/Modal/UploadModal/modalMessage/modalMessage";
import Button from "../../../components/CustomButtons/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import DnsIcon from '@material-ui/icons/Dns';
import HttpsIcon from '@material-ui/icons/Https';

const useStyles = makeStyles(styles);

export default function LoginPage() {

    const [name, setName] = React.useState("");
    const [nameState, setNameState] = React.useState("");

    const [maxSize, setMaxSize] = React.useState("");
    const [maxSizeState, setMaxSizeState] = React.useState("");

    const [modalActive, setModalActive] = React.useState(false);


    const clickHandler = () => {

        if (nameState === "" || nameState === "error") {
            setNameState ("error");
        }
        if (maxSizeState === "" || maxSizeState === "error" ) {
            setMaxSizeState ("error");
        }
        else {
            sendData()
        }
    }

    const onClose = () => {
        setModalActive(false)
        window.location.reload(true);
    }

    const sendData = () => {
        axios.post("http://localhost:8080/addVolume", {
            "volRoot": "/Applications/DOSM/" + name,
            "maxSize" : maxSize
        }).then((response) => {
            if (response.data["errorCode"] === "0") {
                alert("success")
            } else {
                alert("error")
            }
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
    }

    const verifyLength = (value, length) => {
        if (value.length >= length) {
            return true;
        }
        return false;
    };

    const verifyNumber = (value) => {
        return (/^\d+$/.test(value))
    }

    const classes = useStyles();

    return (
        <div className={classes.container}>
            <Modal show={modalActive}>
                < ModalMessage title={"Uh Oh!"} message={"An error occurred"} buttonMessage={"Try again"} closed={onClose}/>
            </Modal>
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={10}>
                    <Card className={classes.cardSignup}>
                        <h2 className={classes.cardTitle}>Create A Volume</h2>
                        <CardBody>
                            <GridContainer justify="center">
                                <GridItem xs={12} sm={8} md={5}>
                                    <form className={classes.form}>
                                        <CustomInput
                                            success={nameState === "success"}
                                            error={nameState === "error"}
                                            id="name"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <DnsIcon className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setNameState("success");
                                                    } else {
                                                        setNameState("error");
                                                    }
                                                    setName(event.target.value);
                                                },
                                                placeholder: "Volume Name"
                                            }}
                                        />
                                        <CustomInput
                                            success={maxSizeState === "success"}
                                            error={maxSizeState === "error"}
                                            id="maxSize"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <HttpsIcon className={classes.inputAdornmentIcon}>
                                                            lock_outline
                                                        </HttpsIcon>
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1) && verifyNumber(event.target.value)) {
                                                        setMaxSizeState("success");
                                                    } else {
                                                        setMaxSizeState("error");
                                                    }
                                                    setMaxSize(event.target.value);
                                                },
                                                placeholder: "Maximum Size"
                                            }}
                                        />
                                        <div className={classes.center}>
                                            <Button round color="primary" onClick={clickHandler}>
                                                Create
                                            </Button>
                                        </div>
                                    </form>
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
