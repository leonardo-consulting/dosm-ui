import React, {useCallback, useContext} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import axios from "axios";

import DropdownMenu from "views/UI/DropdownMenu/DropdownMenu"
import Button from "components/CustomButtons/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import DnsIcon from "@material-ui/icons/Dns";
import DescriptionIcon from '@material-ui/icons/Description';
import {GlobalUserIDContext} from "../../../GlobalUserIDContextManager";


const useStyles = makeStyles(styles);

export default function SelectDomain(props) {

    const classes = useStyles();
    const [domainIds, setDomainIds] = React.useState([])
    const [domainNames, setDomainNames] = React.useState([])
    const [renderDropdown, setRenderDropdown] = React.useState(false)
    const [selectedDomain, setSelectedDomain] = React.useState("");

    const [name, setName] = React.useState("");
    const [nameState, setNameState] = React.useState("");

    const [description, setDescription] = React.useState("");
    const [descriptionState, setDescriptionState] = React.useState("");


    const wrapperSetSelectedDomain = useCallback(val => {
        setSelectedDomain(val);
    }, [setSelectedDomain]);

    const clickHandler = () => {

        if (nameState === "" || nameState === "error") {
            setNameState ("error");
        }
        if (descriptionState === "" || descriptionState === "error" ) {
            setDescriptionState ("error");
        }
        if (selectedDomain === "") {
            alert("Please select a domain")
        }
        else {
            sendData()
        }
    }

    const verifyLength = (value, length) => {
        if (value.length >= length) {
            return true;
        }
        return false;
    };

    const sendData = () => {

        axios.post("http://localhost:8080/updateDomain",
            {
                "globalBDID": selectedDomain,
                "bdName": name,
                "description" : description
            }).then((response) => {
            console.log(response.data)
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
    }


    React.useEffect(() => {
        axios.get("http://localhost:8080/getDomains").then((response) => {
            const dataArray = Object.values(response.data);
            let ids = []
            let names = []
            for (let i = 0; i < dataArray.length; i++) {
                ids.push(dataArray[i]["globalBDID"])
                names.push(dataArray[i]["bdName"])
            }
            setDomainIds(ids)
            setDomainNames(names)
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
        setRenderDropdown(true);
    }, []);

    const renderMenu = () => {
        if (renderDropdown) {
            return (
                <DropdownMenu name={"Domain"} names={domainNames} ids={domainIds} parentStateSetter={wrapperSetSelectedDomain}/>
            )
        }
    }

    return (
        <div className={classes.container}>
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={10}>
                    <Card className={classes.cardSignup}>
                        <h2 className={classes.cardTitle}>Update A Domain</h2>
                        <CardBody>
                            <GridContainer justify="center">
                                <GridItem xs={12} sm={8} md={5}>
                                    {renderMenu()}
                                    <form className={classes.form}>
                                        <CustomInput
                                            success={nameState === "success"}
                                            error={nameState === "error"}
                                            id="name"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <DnsIcon className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setNameState("success");
                                                    } else {
                                                        setNameState("error");
                                                    }
                                                    setName(event.target.value);
                                                },
                                                placeholder: "New Domain Name"
                                            }}
                                        />
                                        <CustomInput
                                            success={descriptionState === "success"}
                                            error={descriptionState === "error"}
                                            id="maxSize"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <DescriptionIcon  className={classes.inputAdornmentIcon}>
                                                            lock_outline
                                                        </DescriptionIcon >
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setDescriptionState("success");
                                                    } else {
                                                        setDescriptionState("error");
                                                    }
                                                    setDescription(event.target.value);
                                                },
                                                placeholder: "New Description"
                                            }}
                                        />
                                    </form>
                                    <div className={classes.center}>
                                        <Button round color="primary" onClick={clickHandler}>
                                            Go
                                        </Button>
                                    </div>
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
