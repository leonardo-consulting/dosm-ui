import React, {useCallback, useContext} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { Redirect } from "react-router-dom";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import axios from "axios";

import Modal from "views/UI/Modal/Modal";
import ModalMessage from "../../UI/Modal/UploadModal/modalMessage/modalMessage";
import Button from "../../../components/CustomButtons/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
import Face from "@material-ui/icons/Face";
import DropdownMenu from "../../UI/DropdownMenu/DropdownMenu";
import {GlobalBDIDContext} from "../../../GlobalBDIDContextManager";
import Checkbox from "@material-ui/core/Checkbox";
import CircleUnchecked from "@material-ui/icons/RadioButtonUnchecked";
import CircleCheckedFilled from "@material-ui/icons/CheckCircle";
import {GlobalAdminContext} from "../../../GlobalAdminContextManager";
import {GlobalUserIDContext} from "../../../GlobalUserIDContextManager";

const useStyles = makeStyles(styles);

export default function LoginPage() {

    const [registerRedirect, setregisterRedirect] = React.useState(false);

    const [registerUsername, setregisterUsername] = React.useState("");
    const [registerUsernameState, setregisterUsernameState] = React.useState("");

    const [registerPassword, setregisterPassword] = React.useState("");
    const [registerPasswordState, setregisterPasswordState] = React.useState("");

    const [modalActive, setModalActive] = React.useState(false);

    const [domainIds, setDomainIds] = React.useState([])
    const [domainNames, setDomainNames] = React.useState([]);
    const [renderDropdown, setRenderDropdown] = React.useState(false);
    const [selectedDomain, setSelectedDomain] = React.useState("");

    const [loginAsAdmin, setLoginAsAdmin] = React.useState(false);

    const wrapperSetSelectedDomain = useCallback(val => {
        setSelectedDomain(val);
    }, [setSelectedDomain]);


    const clickHandler = () => {

        if (registerUsernameState === "" || registerUsernameState === "error") {
            setregisterUsernameState ("error");
        }
        if (registerPasswordState === "" || registerPasswordState === "error" ) {
            setregisterPasswordState ("error");
        }
        if (selectedDomain === "") {
            alert("Please select a domain")
        }
        else {
            sendData()
        }
    }

    const state = useContext(GlobalBDIDContext)
    const adminState = useContext(GlobalAdminContext)
    const userIDState = useContext(GlobalUserIDContext)

    const sendData = () => {
        axios.post("http://localhost:8080/loginToDomain",
            {
                "username": registerUsername,
                "globalBDID" : selectedDomain,
                "password": registerPassword
            }).then((response) => {
            if (response.data["errorCode"] === "250") {
                state.setGlobalBDIDContext(selectedDomain)
                adminState.setGlobalAdminContext(loginAsAdmin)
                userIDState.setGlobalUserIDContext(response.data["globalUserID"])
                setregisterRedirect(true)
            } else {
                alert("Error")
                console.log(response)
            }
        }, (error) => {
            console.log("Error")
            console.log(error);
        });
    }

    const onClose = () => {
        setModalActive(false)
        window.location.reload(true);
    }

    const onChange = () => {
        setLoginAsAdmin(!loginAsAdmin)
    }

    const renderRedirect = () => {
        if (registerRedirect) {
            return (
                <Redirect to={"/user/search"} />
            )
        }
    }

    const renderMenu = () => {
        if (renderDropdown) {
            return (
                <DropdownMenu name={"Domain"} names={domainNames} ids={domainIds} parentStateSetter={wrapperSetSelectedDomain}/>
            )
        }
    }

    React.useEffect(() => {
        axios.get("http://localhost:8080/getDomains").then((response) => {
            const dataArray = Object.values(response.data);
            let ids = []
            let names = []
            for (let i = 0; i < dataArray.length; i++) {
                ids.push(dataArray[i]["globalBDID"])
                names.push(dataArray[i]["bdName"])
            }
            setDomainIds(ids)
            setDomainNames(names)
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
        setRenderDropdown(true);
    }, []);

    const verifyLength = (value, length) => {
        if (value.length >= length) {
            return true;
        }
        return false;
    };

    const classes = useStyles();

    return (
        <div className={classes.container}>
            <Modal show={modalActive}>
                < ModalMessage title={"Uh Oh!"} message={"Your username or password was incorrect"} buttonMessage={"Try again"} closed={onClose}/>
            </Modal>
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={10}>
                    <Card className={classes.cardSignup}>
                        <h2 className={classes.cardTitle}>Login To Domain</h2>
                        <CardBody>
                            <GridContainer justify="center">
                                <GridItem xs={12} sm={8} md={5}>
                                    <form className={classes.form}>
                                        <CustomInput
                                            success={registerUsernameState === "success"}
                                            error={registerUsernameState === "error"}
                                            id="username"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <Face className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterUsernameState("success");
                                                    } else {
                                                        setregisterUsernameState("error");
                                                    }
                                                    setregisterUsername(event.target.value);
                                                },
                                                placeholder: "Username"
                                            }}
                                        />
                                        <CustomInput
                                            success={registerPasswordState === "success"}
                                            error={registerPasswordState === "error"}
                                            id="password"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <Icon className={classes.inputAdornmentIcon}>
                                                            lock_outline
                                                        </Icon>
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterPasswordState("success");
                                                    } else {
                                                        setregisterPasswordState("error");
                                                    }
                                                    setregisterPassword(event.target.value);
                                                },
                                                placeholder: "Password"
                                            }}
                                        />
                                        <div className={classes.center}>

                                            {renderRedirect()}
                                            <div className={"CenterDiv"}>
                                                {renderMenu()}
                                                <Checkbox
                                                    icon={<CircleUnchecked />}
                                                    checkedIcon={<CircleCheckedFilled />}
                                                    checked={loginAsAdmin}
                                                    onChange={onChange}
                                                />
                                                <p className={"Label"}> Login As Admin </p>
                                            </div>
                                            <Button round color="primary" onClick={clickHandler}>
                                                Let's go
                                            </Button>
                                        </div>
                                    </form>
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
