import React, {useCallback, useContext} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
import axios from "axios";

// @material-ui/icons
import Face from "@material-ui/icons/Face";
import Email from "@material-ui/icons/Email";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import CustomInput from "components/CustomInput/CustomInput.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import {Redirect} from "react-router-dom";
import Modal from "../../UI/Modal/Modal";
import ModalMessage from "../../UI/Modal/UploadModal/modalMessage/modalMessage";
import DropdownMenu from "../../UI/DropdownMenu/DropdownMenu";
import {GlobalUserIDContext} from "../../../GlobalUserIDContextManager";

const useStyles = makeStyles(styles);

export default function RegisterPage() {

    const [registerRedirect, setregisterRedirect] = React.useState(false);

    const [registerEmail, setregisterEmail] = React.useState("");
    const [registerEmailState, setregisterEmailState] = React.useState("");

    const [registerPassword, setregisterPassword] = React.useState("");
    const [registerPasswordState, setregisterPasswordState] = React.useState("");

    const [registerUsername, setregisterUsername] = React.useState("");
    const [registerUsernameState, setregisterUsernameState] = React.useState("");

    const [registerDescription, setregisterDescription] = React.useState("");
    const [registerDescriptionState, setregisterDescriptionState] = React.useState("");

    const [registerModalMessage, setregisterModalMessage] = React.useState("");
    const [modalActive, setmodalActive] = React.useState(false);

    const [domainNames, setDomainNames] = React.useState([]);
    const [domainIds, setDomainIds] = React.useState([]);
    const [renderDropdown, setRenderDropdown] = React.useState(false);
    const [selectedDomain, setSelectedDomain] = React.useState("");


    const userIDState = useContext(GlobalUserIDContext)

    const wrapperSetSelectedDomain = useCallback(val => {
        setSelectedDomain(val);
    }, [setSelectedDomain]);

    const verifyLength = (value, length) => {
        if (value.length >= length) {
            return true;
        }
        return false;
    };

    const renderRedirect = () => {

        if (registerRedirect) {
            return (
                <Redirect to={"/user/upload-object"} />
            )
        }
    };

    const onClose = () => {
        setmodalActive(false)
        window.location.reload(true);
    }

    React.useEffect(() => {
        console.log("Yes")
        axios.get("http://localhost:8080/getDomains").then((response) => {
            const dataArray = Object.values(response.data);
            let ids = []
            let names = []
            for (let i = 0; i < dataArray.length; i++) {
                ids.push(dataArray[i]["globalBDID"])
                names.push(dataArray[i]["bdName"])
            }
            setDomainIds(ids)
            setDomainNames(names)
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
        setRenderDropdown(true);
    }, []);

    const renderMenu = () => {
        if (renderDropdown) {
            return (
                <DropdownMenu name={"Domain"} names={domainNames} ids={domainIds} parentStateSetter={wrapperSetSelectedDomain}/>
            )
        }
    }

    const clickHandler = () => {

        if (registerUsernameState === "" || registerUsernameState === "error") {
            setregisterUsernameState ("error");
        }
        if (registerEmailState === "" || registerEmailState === "error" ) {
            setregisterEmailState ("error");
        }
        if (registerPasswordState === "" || registerPasswordState === "error") {
            setregisterPasswordState ("error");
        }
        else {
            sendData()
        }
    }

    const sendData = () => {

        axios.post("http://localhost:8080/addUser", {
            "username": registerUsername,
            "password": registerPassword,
            "systemID": "16",
            "globalBDID": selectedDomain,
            "description": registerDescription,
            "isAdmin": "1",
            "multiLogin": "0",
            "email": registerEmail,
            "status": "0",
            "adminUserID" : userIDState.globalUserID
        }).then((response) => {
            if (response.data["errorCode"] === "0") {
                setregisterRedirect(true);
            } else if (response.data["errorCode"]=== "300") {
                setregisterModalMessage("User already exists");
                setmodalActive(true);
            } else {
                setregisterModalMessage("An error occurred");
                setmodalActive(true);
                console.log(response);
            }}, (error) => {
                console.log("Error")
                console.log(error);
            }
        )
    }


    const classes = useStyles();
    return (
        <div className={classes.container}>
            <Modal show={modalActive}>
                < ModalMessage title={"Uh Oh!"} message={registerModalMessage} buttonMessage={"Close"} closed={onClose}/>
            </Modal>
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={10}>
                    <Card className={classes.cardSignup}>
                        <h2 className={classes.cardTitle}>Add User To Domain</h2>
                        <CardBody>
                            <GridContainer justify="center">
                                <GridItem xs={12} sm={8} md={5}>
                                    <form className={classes.form}>
                                        <CustomInput
                                            success={registerUsernameState === "success"}
                                            error={registerUsernameState === "error"}
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <Face className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterUsernameState("success");
                                                    } else {
                                                        setregisterUsernameState("error");
                                                    }
                                                    setregisterUsername(event.target.value);
                                                },
                                                placeholder: "Username"
                                            }}
                                        />
                                        <CustomInput
                                            success={registerEmailState === "success"}
                                            error={registerEmailState === "error"}
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <Email className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterEmailState("success");
                                                    } else {
                                                        setregisterEmailState("error");
                                                    }
                                                    setregisterEmail(event.target.value);
                                                },
                                                placeholder: "Email"
                                            }}
                                        />
                                        <CustomInput
                                            success={registerPasswordState === "success"}
                                            error={registerPasswordState === "error"}
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <Icon className={classes.inputAdornmentIcon}>
                                                            lock_outline
                                                        </Icon>
                                                    </InputAdornment>
                                                ), onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterPasswordState("success");
                                                    } else {
                                                        setregisterPasswordState("error");
                                                    }
                                                    setregisterPassword(event.target.value);
                                                },
                                                placeholder: "Password"
                                            }}
                                        />
                                        <CustomInput
                                            success={registerDescriptionState === "success"}
                                            error={registerDescriptionState === "error"}
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <Email className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterDescriptionState("success");
                                                    } else {
                                                        setregisterDescriptionState("error");
                                                    }
                                                    setregisterDescription(event.target.value);
                                                },
                                                placeholder: "Description"
                                            }}
                                        />
                                        {renderMenu()}
                                        <div className={classes.center}>
                                            {renderRedirect()}
                                            <Button round color="primary" onClick={clickHandler}>
                                                Add User
                                            </Button>
                                        </div>
                                    </form>
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
