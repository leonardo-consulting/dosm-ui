import React, {useCallback, useContext} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import axios from "axios";

import DropdownMenu from "views/UI/DropdownMenu/DropdownMenu"
import Button from "components/CustomButtons/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import Face from "@material-ui/icons/Face";
import {GlobalUserIDContext} from "../../../GlobalUserIDContextManager";


const useStyles = makeStyles(styles);

export default function SelectDomain(props) {

    const classes = useStyles();
    const [domainIds, setDomainIds] = React.useState([])
    const [domainNames, setDomainNames] = React.useState([])
    const [renderDropdown, setRenderDropdown] = React.useState(false)
    const [selectedDomain, setSelectedDomain] = React.useState("");

    const [applicationIds, setApplicationIds] = React.useState([])
    const [applicationNames, setApplicationNames] = React.useState([])
    const [renderApplicationDropdown, setApplicationRenderDropdown] = React.useState(false)
    const [selectedApplication, setSelectedApplication] = React.useState("");

    const [userId, setUserId] = React.useState("");
    const [userIdState, setUserIdState] = React.useState("");

    const userIDState = useContext(GlobalUserIDContext)

    const wrapperSetSelectedDomain = useCallback(val => {
        setSelectedDomain(val);
    }, [setSelectedDomain]);

    const wrapperSetSelectedApplication = useCallback(val => {
        setSelectedApplication(val)
    }, [setSelectedApplication])

    const clickHandler = () => {

        if (selectedApplication === "") {
            alert("Please select a domain")
        }
        else if (selectedDomain === "") {
            alert("Please select a domain")
        }
        else {
            sendData()
        }
    }

    const sendData = () => {

        const DEBUG = process.env.NODE_ENV === "development";

        axios.interceptors.request.use((config) => {

            if (DEBUG) { console.info("✉️ ", config.data); }
            return config;
        }, (error) => {
            if (DEBUG) { console.error("✉️ ", error); }
            return Promise.reject(error);
        });

        axios.post("http://localhost:8080/addUserToApplication",
            {
                "globalAppID": selectedApplication,
                "globalUserID" : userId,
                "adminUserID" : userIDState.globalUserID
            }).then((response) => {
            if (response.data["errorCode"] === "0") {
                alert("success")
            } else {
                alert("error")
            }
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
    }


    React.useEffect(() => {
        axios.get("http://localhost:8080/getDomains").then((response) => {
            const dataArray = Object.values(response.data);
            let ids = []
            let names = []
            for (let i = 0; i < dataArray.length; i++) {
                ids.push(dataArray[i]["globalBDID"])
                names.push(dataArray[i]["bdName"])
            }
            setDomainIds(ids)
            setDomainNames(names)
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
        setRenderDropdown(true);
    }, []);

    React.useEffect(() => {
        if (selectedDomain !== "") {
            axios.post("http://localhost:8080/getAllApplications", {
                "globalbdid": selectedDomain
            }).then((response) => {
                const dataArray = Object.values(response.data);
                let ids = []
                let names = []
                for (let i = 0; i < dataArray.length; i++) {
                    ids.push(dataArray[i]["globalappid"])
                    names.push(dataArray[i]["appdescription"])
                }
                setApplicationIds(ids)
                setApplicationNames(names)
            }, (error) => {
                console.log("Error")
                console.log(error);
            })
            setApplicationRenderDropdown(true);
        }
    }, [selectedDomain]);

    const renderMenu = () => {
        if (renderDropdown) {
            return (
                <DropdownMenu name={"Domain"} names={domainNames} ids={domainIds} parentStateSetter={wrapperSetSelectedDomain}/>
            )
        }
    }

    const renderApplicationMenu = () => {
        if (renderApplicationDropdown) {
            return (
                <DropdownMenu name={"Application"} names={applicationNames} ids={applicationIds} parentStateSetter={wrapperSetSelectedApplication}/>
            )
        }
    }

    const verifyLength = (value, length) => {
        if (value.length >= length) {
            return true;
        }
        return false;
    };

    return (
        <div className={classes.container}>
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={10}>
                    <Card className={classes.cardSignup}>
                        <h2 className={classes.cardTitle}>Add Volume To Domain</h2>
                        <CardBody>
                            <GridContainer justify="center">
                                <GridItem xs={12} sm={8} md={5}>
                                    <CustomInput
                                        success={userIdState === "success"}
                                        error={userIdState === "error"}
                                        id="User id"
                                        formControlProps={{
                                            fullWidth: true,
                                            className: classes.customFormControlClasses
                                        }}
                                        inputProps={{
                                            startAdornment: (
                                                <InputAdornment
                                                    position="start"
                                                    className={classes.inputAdornment}
                                                >
                                                    <Face className={classes.inputAdornmentIcon} />
                                                </InputAdornment>
                                            ),
                                            onChange: event => {
                                                if (verifyLength(event.target.value, 1)) {
                                                    setUserIdState("success");
                                                } else {
                                                    setUserIdState("error");
                                                }
                                                setUserId(event.target.value);
                                            },
                                            placeholder: "Global User Id"
                                        }}
                                    />
                                    {renderMenu()}
                                    {renderApplicationMenu()}
                                    <div className={classes.center}>
                                        <Button round color="primary" onClick={clickHandler}>
                                            Add
                                        </Button>
                                    </div>
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
