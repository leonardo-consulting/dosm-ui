import React, {useCallback, useContext} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import axios from "axios";

import DropdownMenu from "views/UI/DropdownMenu/DropdownMenu"
import Button from "components/CustomButtons/Button";
import {GlobalUserIDContext} from "../../../GlobalUserIDContextManager";


const useStyles = makeStyles(styles);

export default function SelectDomain(props) {

    const classes = useStyles();
    const [domainIds, setDomainIds] = React.useState([])
    const [domainNames, setDomainNames] = React.useState([])
    const [renderDropdown, setRenderDropdown] = React.useState(false)
    const [selectedDomain, setSelectedDomain] = React.useState("");

    const [volumeIds, setVolumeIds] = React.useState([])
    const [volumeNames, setVolumeNames] = React.useState([])
    const [renderVolumeDropdown, setVolumeRenderDropdown] = React.useState(false)
    const [selectedVolume, setSelectedVolume] = React.useState("");

    const userIDState = useContext(GlobalUserIDContext)

    const wrapperSetSelectedDomain = useCallback(val => {
        setSelectedDomain(val);
    }, [setSelectedDomain]);

    const wrapperSetSelectedVolume = useCallback(val => {
        setSelectedVolume(val)
    }, [setSelectedVolume])

    const clickHandler = () => {

        if (selectedVolume === "") {
            alert("Please select a domain")
        }
        else if (selectedDomain === "") {
            alert("Please select a domain")
        }
        else {
            sendData()
        }
    }

    const sendData = () => {

        const DEBUG = process.env.NODE_ENV === "development";

        axios.interceptors.request.use((config) => {

            if (DEBUG) { console.info("✉️ ", config.data); }
            return config;
        }, (error) => {
            if (DEBUG) { console.error("✉️ ", error); }
            return Promise.reject(error);
        });

        axios.post("http://localhost:8080/addVolumeToDomain",
            {
            "globalBDID": selectedDomain,
            "globalVolID" : selectedVolume,
            "adminUserID" : userIDState.globalUserID
            }).then((response) => {
            if (response.data["errorCode"] === "0") {
                alert("success")
            } else {
                alert("error")
            }
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
    }


    React.useEffect(() => {
        axios.get("http://localhost:8080/getDomains").then((response) => {
            const dataArray = Object.values(response.data);
            let ids = []
            let names = []
            for (let i = 0; i < dataArray.length; i++) {
                ids.push(dataArray[i]["globalBDID"])
                names.push(dataArray[i]["bdName"])
            }
            setDomainIds(ids)
            setDomainNames(names)
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
        setRenderDropdown(true);
    }, []);



    const parseName = (val) => {
        let i;
        for (i = val.length - 1; i >= 0; i--) {
            if (val[i] === '/') {
                break;
            }
        }
        return val.slice(i+1)
    }


    React.useEffect(() => {
        axios.get("http://localhost:8080/getAllVolumes").then((response) => {
            const dataArray = Object.values(response.data);
            let ids = []
            let names = []
            for (let i = 0; i < dataArray.length; i++) {
                ids.push(dataArray[i]["globalVolID"])
                names.push(parseName(dataArray[i]["volRoot"]))
            }
            setVolumeIds(ids)
            setVolumeNames(names)
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
        setVolumeRenderDropdown(true);
    }, []);

    const renderMenu = () => {
        if (renderDropdown) {
            return (
                <DropdownMenu name={"Domain"} names={domainNames} ids={domainIds} parentStateSetter={wrapperSetSelectedDomain}/>
            )
        }
    }

    const renderVolumeMenu = () => {
        if (renderVolumeDropdown) {
            return (
                <DropdownMenu name={"Volume"} names={volumeNames} ids={volumeIds} parentStateSetter={wrapperSetSelectedVolume}/>
            )
        }
    }

    return (
        <div className={classes.container}>
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={10}>
                    <Card className={classes.cardSignup}>
                        <h2 className={classes.cardTitle}>Add Volume To Domain</h2>
                        <CardBody>
                            <GridContainer justify="center">
                                <GridItem xs={12} sm={8} md={5}>
                                    {renderVolumeMenu()}
                                    {renderMenu()}
                                    <div className={classes.center}>
                                        <Button round color="primary" onClick={clickHandler}>
                                            Add
                                        </Button>
                                    </div>
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
