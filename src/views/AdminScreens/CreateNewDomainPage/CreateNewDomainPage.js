import React, {useContext} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CustomInput from "components/CustomInput/CustomInput.js";

import styles from "assets/jss/material-dashboard-pro-react/views/registerPageStyle";
import axios from "axios";

import Modal from "views/UI/Modal/Modal";
import ModalMessage from "../../UI/Modal/UploadModal/modalMessage/modalMessage";
import Button from "../../../components/CustomButtons/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import Icon from "@material-ui/core/Icon";
import Face from "@material-ui/icons/Face";
import DescriptionIcon from '@material-ui/icons/Description';
import DnsIcon from '@material-ui/icons/Dns';
import {GlobalUserIDContext} from "../../../GlobalUserIDContextManager";


const useStyles = makeStyles(styles);

export default function LoginPage() {

    const [registerDomainName, setregisterDomainName] = React.useState("");
    const [registerDomainNameState, setregisterDomainNameState] = React.useState("");

    const [registerDomainDescription, setregisterDomainDescription] = React.useState("");
    const [registerDomainDescriptionState, setregisterDomainDescriptionState] = React.useState("");

    const [registerUsername, setregisterUsername] = React.useState("");
    const [registerUsernameState, setregisterUsernameState] = React.useState("");

    const [registerPassword, setregisterPassword] = React.useState("");
    const [registerPasswordState, setregisterPasswordState] = React.useState("");

    const [registerUserDescription, setregisterUserDescription] = React.useState("");
    const [registerUserDescriptionState, setregisterUserDescriptionState] = React.useState("");

    const [modalActive, setModalActive] = React.useState(false);

    const userIDState = useContext(GlobalUserIDContext)


    const clickHandler = () => {

        if (registerUsernameState === "" || registerUsernameState === "error") {
            setregisterUsernameState ("error");
        }
        if (registerPasswordState === "" || registerPasswordState === "error" ) {
            setregisterPasswordState ("error");
        }
        if (registerDomainNameState === "" || registerDomainNameState === "error") {
            setregisterDomainNameState ("error");
        }
        if (registerDomainDescriptionState === "" || registerDomainDescriptionState === "error" ) {
            setregisterDomainDescriptionState ("error");
        }
        if (registerUserDescriptionState === "" || registerUserDescriptionState === "error") {
            setregisterUserDescriptionState("error")
        }
        else {
            sendData();
        }
    }

    const onClose = () => {
        setModalActive(false)
        window.location.reload(true);
    }

    const sendData = () => {
        axios.post("http://localhost:8080/createNewDomain",
            {
                "bdName": registerDomainName,
                "description" : registerDomainDescription,
                "username": registerUsername,
                "password": registerPassword,
                "userDescription": registerUserDescription,
                "createFirstUser": true,
                "adminUserID" : userIDState.globalUserID
            }).then((response) => {
            if (response.data["errorCode"] === "0") {
                alert("success")
            } else {
                alert("error")
                console.log(response)
            }
        }, (error) => {
            console.log("Error")
            console.log(error);
        })
    }

    const verifyLength = (value, length) => {
        if (value.length >= length) {
            return true;
        }
        return false;
    };

    const classes = useStyles();

    return (
        <div className={classes.container}>
            <Modal show={modalActive}>
                <ModalMessage title={"Uh Oh!"} message={"An error occurred"} buttonMessage={"Close"} closed={onClose}/>
            </Modal>
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={10}>
                    <Card className={classes.cardSignup}>
                        <h2 className={classes.cardTitle}>Create A New Domain</h2>
                        <CardBody>
                            <GridContainer justify="center">
                                <GridItem xs={12} sm={8} md={5}>
                                    <form className={classes.form}>
                                        <CustomInput
                                            success={registerDomainNameState === "success"}
                                            error={registerDomainNameState === "error"}
                                            id="domainName"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <DnsIcon className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterDomainNameState("success");
                                                    } else {
                                                        setregisterDomainNameState("error");
                                                    }
                                                    setregisterDomainName(event.target.value);
                                                },
                                                placeholder: "Domain Name"
                                            }}
                                        />
                                        <CustomInput
                                            success={registerDomainDescriptionState === "success"}
                                            error={registerDomainDescriptionState === "error"}
                                            id="username"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <DescriptionIcon className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterDomainDescriptionState("success");
                                                    } else {
                                                        setregisterDomainDescriptionState("error");
                                                    }
                                                    setregisterDomainDescription(event.target.value);
                                                },
                                                placeholder: "Domain Description"
                                            }}
                                        />
                                        <CustomInput
                                            success={registerUsernameState === "success"}
                                            error={registerUsernameState === "error"}
                                            id="username"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <Face className={classes.inputAdornmentIcon} />
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterUsernameState("success");
                                                    } else {
                                                        setregisterUsernameState("error");
                                                    }
                                                    setregisterUsername(event.target.value);
                                                },
                                                placeholder: "Admin Username"
                                            }}
                                        />
                                        <CustomInput
                                            success={registerPasswordState === "success"}
                                            error={registerPasswordState === "error"}
                                            id="password"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <Icon className={classes.inputAdornmentIcon}>
                                                            lock_outline
                                                        </Icon>
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterPasswordState("success");
                                                    } else {
                                                        setregisterPasswordState("error");
                                                    }
                                                    setregisterPassword(event.target.value);
                                                },
                                                placeholder: "Admin Password"
                                            }}
                                        />
                                        <CustomInput
                                            success={registerUserDescriptionState === "success"}
                                            error={registerUserDescriptionState === "error"}
                                            id="User description"
                                            formControlProps={{
                                                fullWidth: true,
                                                className: classes.customFormControlClasses
                                            }}
                                            inputProps={{
                                                startAdornment: (
                                                    <InputAdornment
                                                        position="start"
                                                        className={classes.inputAdornment}
                                                    >
                                                        <DescriptionIcon className={classes.inputAdornmentIcon}>
                                                            lock_outline
                                                        </DescriptionIcon>
                                                    </InputAdornment>
                                                ),
                                                onChange: event => {
                                                    if (verifyLength(event.target.value, 1)) {
                                                        setregisterUserDescriptionState("success");
                                                    } else {
                                                        setregisterUserDescriptionState("error");
                                                    }
                                                    setregisterUserDescription(event.target.value);
                                                },
                                                placeholder: "User Description"
                                            }}
                                        />
                                        <div className={classes.center}>
                                            <Button round color="primary" onClick={clickHandler}>
                                                Create!
                                            </Button>
                                        </div>
                                    </form>
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    );
}
