/*eslint-disable*/
import React, {useCallback, useContext, useEffect} from "react";
import axios from 'axios';

import { makeStyles } from "@material-ui/core/styles";

import Close from "@material-ui/icons/Close";

// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";

// style for this view
import styles from "assets/jss/material-dashboard-pro-react/views/validationFormsStyle.js";

import Modal from "views/UI/Modal/Modal.js";
import ModalMessage from "views/UI/Modal/UploadModal/modalMessage/modalMessage.js";
import DownloadButton from "../SearchResults/DownloadButton";
import {GlobalAPPIDContext} from "../../../GlobalAPPIDContextManager";
import {GlobalBDIDContext} from "../../../GlobalBDIDContextManager";
import DateRange from "../Dates/DateRange";
import ExactDate from "../Dates/ExactDate";
import Checkbox from "@material-ui/core/Checkbox";


const useStyles = makeStyles(styles);

export default function LikeSearch({parentStateSetter}) {
    // register form

    const [uploadModalActive, setuploadModalActive] = React.useState(false);
    const [modalTitle, setmodalTitle] = React.useState("");
    const [modalMessage, setmodalMessage] = React.useState("");

    const [tableData, setTableData] = React.useState([[]]);

    const [checked, setChecked] = React.useState(false);
    const [checkedExact, setCheckedExact] = React.useState(false);
    const [searchLogic, setSearchLogic] = React.useState("LIKE");
    const [dateField, setDateField] = React.useState({})

    const appidState = useContext(GlobalAPPIDContext)
    const bdidState = useContext(GlobalBDIDContext)

    const handleChange = (event) => {
        setChecked(event.target.checked);
    }

    const handleChangeExact = (event) => {
        if (checkedExact) {
            setSearchLogic("LIKE")
        } else {
            setSearchLogic("EXACT")
        }
        setCheckedExact(event.target.checked);
    }

    useEffect(() => {
        parentStateSetter(tableData);
    }, [parentStateSetter, tableData]);

    // function that verifies if a string has a given length or not
    const verifyLength = (value, length) => {
        if (value.length <= length) {
            return true;
        }
        return false;
    };

    const renderDate = (field) => {
        if (checked) {
            return (<DateRange parentStateSetter={wrapperSetDateField} key={field.name}/>)
        } else {
            return (<ExactDate parentStateSetter={wrapperSetDateField} key={field.name}/>)
        }
    }

    const wrapperSetDateField = useCallback(val => {
        setDateField(val);
    }, [setDateField]);

    useEffect(() => {
        axios.get("http://localhost:8080/getfields?appid=" + appidState.globalAPPID)
            .then((response) => {
                console.log(response)
                if (response.status == 200) {
                    setFields(response.data)
                } else {alert("error")
                }
            }, (error) => {
                alert("error")
            });

    }, [appidState.globalAPPID]);

    const setFields = (data) => {
        setFormData([]);
        for (let i = 0; i < data.length; i++) {
            const newField = {
                name: data[i]["fieldName"],
                length: data[i]["fieldLength"],
                type: data[i]["fieldType"],
                state: "",
                value: "",
            }
            setFormData(formData => [...formData, newField])
        }
    }

    const setValueField = (name, value) => {

        const data = [...formData];
        const index = data.findIndex(m => m.name === name);
        data[index].value = value;

        setFormData(data)
    }

    const setStateField = (name, value) => {
        const data = [...formData];
        const index = data.findIndex(m => m.name === name);
        data[index].state = value;

        setFormData(data)
    }

    const [formData, setFormData] = React.useState([{}])

    const formatResultFields = () => {
        let resultFields = []
        for (let i = 0; i < formData.length; i++) {
            const newField = {
                "fieldName": formData[i].name,
                "fieldType": formData[i].type,
                "fieldLength": formData[i].length
            }
            resultFields.push(newField)
        }
        return resultFields
    }

    const formatDateField = (field) => {
        if (dateField.date) {
            return ({"fieldName" : field.name,
                    "searchVal" : dateField.date,
                    "fieldType" : "2",
                    "fieldLength" : field.length,
                    "searchType" : "STANDARD",
                    "searchLogic" : searchLogic}
            )
        } else {
            return ({"fieldName" : field.name,
                    "dateStart" : dateField.startDate,
                    "dateEnd" : dateField.endDate,
                    "fieldType" : "2",
                    "fieldLength" : field.length,
                    "searchType" : "DATE_RANGE",
                    "searchLogic" : ""}
            )
        }
    }

    const formatSearchFields = () => {
        let searchFields = []
        for (let i = 0; i < formData.length; i++) {
            if (formData[i].type === '2') {
                searchFields.push(formatDateField(formData[i]))
            }
            else {const newField = {
                "fieldName": formData[i].name,
                "searchVal": formData[i].value,
                "fieldType": formData[i].type,
                "fieldLength": formData[i].length,
                "searchType" : "STANDARD",
                "searchLogic" : searchLogic
            }
            searchFields.push(newField)}
        }
        return searchFields
    }

    const sendData = () => {
        axios.post("http://localhost:8080/searchapp",
            { "searchFields" : formatSearchFields(),
            "resultFields" : formatResultFields(),
            "sortBy" : formData[0].name,
            "sortDirection" : "0",
            "startRow" : "0",
            "numberOfRows" : "10",
            "globalAppID" : appidState.globalAPPID})
            .then((response) => {
                if (response.status == 200) {
                    console.log(response)
                    formatTableData(response.data);
                } else {
                    setmodalTitle("Uh oh!")
                    setmodalMessage("An error occurred, please try again")
                    setuploadModalActive(true)
                }
            }, (error) => {
                console.log("Error")
                setmodalTitle("Uh oh!")
                setmodalMessage("An error occured, please try again")
                setuploadModalActive(true)
                console.log(error);
            });
    }

    const formatTableData = (response) => {
        if (response != "" && response["totalResultsInSearch"] != 0) {
            const newtableData = [];
            const fieldNames = ["Object ID"];
            for (let i = 0; i < formData.length; i++) {
                fieldNames.push(formData[i].name)
            }
            newtableData.push(fieldNames)
            for (let i = 0; i < response.rows.length; i++) {
                let newData = []
                newData.push(response.rows[i]["objectID"])
                for (let j = 0; j < response.rows[i]["searchResultFields"].length; j++) {
                    newData.push(response.rows[i]["searchResultFields"][j]["searchResult"])
                }
                newData.push(<DownloadButton objectId={response.rows[i]["objectID"]}></DownloadButton>)
                newtableData.push(newData)
            }

            setTableData(newtableData)
        } else {
            setmodalTitle("Uh oh!")
            setmodalMessage("There are no results for your search...")
            setuploadModalActive(true)
        }
    }

    const registerClick = () => {
        sendData();
    };

    const renderDateField = (field) => {
            return (<div>{renderDate(field)}
                <div className={"CenterDiv"} >
                <p className={"Label"}> Search using date range</p>
            <Checkbox
                checked={checked}
                onChange={handleChange}
                inputProps={{ 'aria-label': 'primary checkbox' }}/>
                </div></div>)
    }

    const renderForm = formData.map((field) => {
        if (field.type === '2') {
                return (renderDateField(field))
            } else {
                return (<CustomInput
                    key={field.name}
                    success={field.state=== "success"}
                    error={field.state === "error"}
                    labelText={field["name"]}
                    id={field["name"]}
                    formControlProps={{
                        fullWidth: true
                    }}
                    inputProps={{
                        onChange: event => {
                            if (verifyLength(event.target.value, field.length) && field.type !== '2') {
                                setStateField(field.name, "success");
                            } else if (field.type === '2' && verifyDate(event.target.value)) {
                                setStateField(field.name, "success");
                            }
                            else {
                                setStateField(field.name, "error");
                            }
                            setValueField(field.name, event.target.value)
                        },
                        type: "keyword",
                        autoComplete: "off"
                    }}
                />
            )}})



    const classes = useStyles();
    return (
        <form>
            <Modal show={uploadModalActive}>
                < ModalMessage title={modalTitle} message={modalMessage} buttonMessage={"Close"}/>
            </Modal>
            {renderForm}
            <Button
                color="rose"
                onClick={registerClick}
                className={classes.registerButton}
            >
                Search
            </Button>
            <div className={"CenterDiv"} >
                <p className={"Label"}> Exact matches only</p>
                <Checkbox
                    checked={checkedExact}
                    onChange={handleChangeExact}
                    inputProps={{ 'aria-label': 'primary checkbox' }}/>
            </div>
        </form>
    );
}