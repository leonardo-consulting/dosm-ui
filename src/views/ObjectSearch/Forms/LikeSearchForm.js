/*eslint-disable*/
import React, {useCallback, useContext, useEffect} from "react";
import axios from 'axios';

import { makeStyles } from "@material-ui/core/styles";

import Close from "@material-ui/icons/Close";

// core components
import CustomInput from "components/CustomInput/CustomInput.js";
import Button from "components/CustomButtons/Button.js";

// style for this view
import styles from "assets/jss/material-dashboard-pro-react/views/validationFormsStyle.js";

import Modal from "views/UI/Modal/Modal.js";
import ModalMessage from "views/UI/Modal/UploadModal/modalMessage/modalMessage.js";
import DownloadButton from "../SearchResults/DownloadButton";
import {GlobalAPPIDContext} from "../../../GlobalAPPIDContextManager";
import {GlobalBDIDContext} from "../../../GlobalBDIDContextManager";


const useStyles = makeStyles(styles);

export default function LikeSearch({parentStateSetter}) {
    // register form
    const [registerKeyword, setregisterKeyword] = React.useState("");
    const [registerKeywordState, setregisterKeywordState] = React.useState("");

    const [uploadModalActive, setuploadModalActive] = React.useState(false);
    const [modalTitle, setmodalTitle] = React.useState("");
    const [modalMessage, setmodalMessage] = React.useState("");

    const [tableData, setTableData] = React.useState([[]]);

    const appidState = useContext(GlobalAPPIDContext)
    const bdidState = useContext(GlobalBDIDContext)

    useEffect(() => {
        parentStateSetter(tableData);
    }, [parentStateSetter, tableData]);

    // function that verifies if a string has a given length or not
    const verifyLength = (value, length) => {
        if (value.length <= length) {
            return true;
        }
        return false;
    };


    useEffect(() => {
        axios.get("http://localhost:8080/getfields?appid=" + appidState.globalAPPID)
            .then((response) => {
                console.log(response)
                if (response.status == 200) {
                    setFields(response.data)
                } else {alert("error")
                }
            }, (error) => {
                alert("error")
            });

    }, [appidState.globalAPPID]);

    const setFields = (data) => {
        setFormData([]);
        for (let i = 0; i < data.length; i++) {
            const newField = {
                name: data[i]["fieldName"],
                length: data[i]["fieldLength"],
                type: data[i]["fieldType"],
            }
            setFormData(formData => [...formData, newField])
        }
    }

    const [formData, setFormData] = React.useState([{}])

    console.log(formData)

    const maxLength = () => {
        let length = 0
        for (let i = 0; i < formData.length; i++) {
            if (formData[i].length > length) {
                length = formData[i].length
            }
        }
        return length
    }

    const formatResultFields = () => {
        let resultFields = []
        for (let i = 0; i < formData.length; i++) {
            const newField = {
                "fieldName": formData[i].name,
                "fieldType": formData[i].type,
                "fieldLength": formData[i].length
            }
            resultFields.push(newField)
        }
        return resultFields
    }


    const sendData = () => {

        const DEBUG = process.env.NODE_ENV === "development";

        axios.interceptors.request.use((config) => {
            /** In dev, intercepts request and logs it into console for dev */
            if (DEBUG) { console.info("✉️ ", config.data); }
            return config;
        }, (error) => {
            if (DEBUG) { console.error("✉️ ", error); }
            return Promise.reject(error);
        });

        axios.post("http://localhost:8080/searchapp", {"searchFields" : [
                {"fieldName" : "",
                    "searchVal" : registerKeyword,
                    "fieldType" : "0",
                    "fieldLength" : maxLength(),
                    "searchType" : "ALL",
                    "searchLogic" : "LIKE"}
            ],
            "resultFields" : formatResultFields(),
            "sortBy" : formData[0].name,
            "sortDirection" : "0",
            "startRow" : "0",
            "numberOfRows" : "10",
            "globalAppID" : appidState.globalAPPID
        }).then((response) => {
            console.log(response)
            if (response.status == 200) {
                formatTableData(response.data);
            } else {
                setmodalTitle("Uh oh!")
                setmodalMessage("An error occurred, please try again")
                setuploadModalActive(true)
            }
        }, (error) => {
            console.log("Error")
            setmodalTitle("Uh oh!")
            setmodalMessage("An error occured, please try again")
            setuploadModalActive(true)
            console.log(error);
        });
    }

    const formatTableData = (response) => {
        if (response != "" && response["totalResultsInSearch"] != 0) {
            const newtableData = [];
            const fieldNames = ["Object ID"];
            for (let i = 0; i < formData.length; i++) {
                fieldNames.push(formData[i].name)
            }
            newtableData.push(fieldNames)
            for (let i = 0; i < response.rows.length; i++) {
                let newData = []
                newData.push(response.rows[i]["objectID"])
                for (let j = 0; j < response.rows[i]["searchResultFields"].length; j++) {
                    newData.push(response.rows[i]["searchResultFields"][j]["searchResult"])
                }
                //const array = [response.rows[i]["objectID"], response.rows[i]["searchResultFields"][0]["searchResult"], response.rows[i]["searchResultFields"][1]["searchResult"], response.rows[i]["searchResultFields"][2]["searchResult"]];
                //tableData.push(array)
                newData.push(<DownloadButton objectId={response.rows[i]["objectID"]}></DownloadButton>)
                {/*axios({
                    url: 'http://localhost:8080/getobject?objectid=' + response.rows[i]["objectID"],
                    method: 'GET',
                    responseType: 'blob',
                }).then((response) => {
                    const fileReaderInstance = new FileReader();
                    fileReaderInstance.readAsDataURL(response.data);
                    fileReaderInstance.onload = () => {
                        let base64data = fileReaderInstance.result;
                        tableData[i].push(base64data)
                    }
                }) */}
                newtableData.push(newData)
            }

            setTableData(newtableData)
        } else {
            setmodalTitle("Uh oh!")
            setmodalMessage("There are no results for your search...")
            setuploadModalActive(true)
        }
    }

    const registerClick = () => {
        sendData();
    };

    const classes = useStyles();
    return (
        <form>
            <Modal show={uploadModalActive}>
                < ModalMessage title={modalTitle} message={modalMessage} buttonMessage={"Close"}/>
            </Modal>
            <CustomInput
                success={registerKeywordState === "success"}
                error={registerKeywordState === "error"}
                labelText="Keyword"
                id="field0"
                formControlProps={{
                    fullWidth: true
                }}
                inputProps={{
                    onChange: event => {
                        if (verifyLength(event.target.value, 30)) {
                            setregisterKeywordState("success");
                        } else {
                            setregisterKeywordState("error");
                        }
                        setregisterKeyword(event.target.value);
                    },
                    type: "keyword",
                    autoComplete: "off"
                }}
            />
            <Button
                color="rose"
                onClick={registerClick}
                className={classes.registerButton}
            >
                Search
            </Button>
        </form>
    );
}