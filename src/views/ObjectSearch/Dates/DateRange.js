import React, {useEffect} from 'react';
import CustomInput from "../../../components/CustomInput/CustomInput";

export default function DateRange({parentStateSetter}) {

    const [registerStartDate, setregisterStartDate] = React.useState("");
    const [registerStartDateState, setregisterStartDateState] = React.useState("");
    const [registerEndDate, setregisterEndDate] = React.useState("");
    const [registerEndDateState, setregisterEndDateState] = React.useState("");
    const [dateField, setDateField] = React.useState({});

    const verifyDate = (value) => {
        var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        return (value.match(reg))
    }

    useEffect(() => {
        parentStateSetter(dateField);
    }, [dateField, parentStateSetter]);

    useEffect(() => setDateField({"start date": registerStartDate, "end date": registerEndDate}
    ), [registerEndDate , registerStartDate]);

    return (
        <div>
            <CustomInput
                success={registerStartDateState === "success"}
                error={registerStartDateState === "error"}
                labelText="Start Date (DD/MM/YYYY)"
                id="Start Date"
                formControlProps={{
                    fullWidth: true
                }}
                inputProps={{
                    onChange: event => {
                        if (verifyDate(event.target.value)) {
                            setregisterStartDateState("success");
                        } else {
                            setregisterStartDateState("error");
                        }
                        setregisterStartDate(event.target.value);
                    },
                    type: "DDMMYYYY",
                    autoComplete: "off"
                }}
            />
            <CustomInput
                success={registerEndDateState === "success"}
                error={registerEndDateState === "error"}
                labelText="End Date (DD/MM/YYYY)"
                id="End Date"
                formControlProps={{
                    fullWidth: true
                }}
                inputProps={{
                    onChange: event => {
                        if (verifyDate(event.target.value) && event.target.value.length === 10) {
                            setregisterEndDateState("success");
                        } else {
                            setregisterEndDateState("error");
                        }
                        setregisterEndDate(event.target.value);
                    },
                    type: "DDMMYYYY",
                    autoComplete: "off"
                }}
            /> </div>
    )
}