import React, {useEffect} from 'react';
import CustomInput from "../../../components/CustomInput/CustomInput";

export default function ExactDate({parentStateSetter}) {

    const [registerDateState, setregisterDateState] = React.useState("");
    const [registerDate, setregisterDate] = React.useState("");
    const [dateField, setDateField] = React.useState({});

    const verifyDate = (value) => {
        var reg = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/;
        return (value.match(reg))
    }

    useEffect(() => {
        parentStateSetter(dateField);
    }, [parentStateSetter, dateField]);

    useEffect(() => setDateField({
        "data": registerDate
    }), [registerDate]);


    const renderDate = () => {

        return(
            <CustomInput
                success={registerDateState === "success"}
                error={registerDateState === "error"}
                labelText="Date (DD/MM/YYYY)"
                id="field2"
                formControlProps={{
                    fullWidth: true
                }}
                inputProps={{
                    onChange: event => {
                        if (verifyDate(event.target.value)) {
                            setregisterDateState("success");
                        } else {
                            setregisterDateState("error");
                        }
                        setregisterDate(event.target.value);
                    },
                    type: "DDMMYYYY",
                    autoComplete: "off"
                }}
            />
        )

    }

    return (<div>{renderDate()}</div>)
}