
import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// material-ui icons
import Assignment from "@material-ui/icons/Assignment";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Table from "components/Table/Table.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardIcon from "components/Card/CardIcon.js";
import CardHeader from "components/Card/CardHeader.js";

import styles from "assets/jss/material-dashboard-pro-react/views/extendedTablesStyle.js";


const useStyles = makeStyles(styles);

export default function ExtendedTables(props) {

  const classes = useStyles();

  const renderTableData = () => {
      let newTableData = []
      for (let i = 1; i < props.tableData.length; i++) {

          newTableData.push(props.tableData[i])
      }
      return (newTableData)
  }

  //  axios({
    //      url: 'http://localhost:8080/getobject?objectid=' + props.tableData[i][0],
    //      method: 'GET',
    //      responseType: 'blob',
  //    }).then((response) => {
     //     const fileReaderInstance = new FileReader();
      //    fileReaderInstance.readAsDataURL(response.data);
       //   fileReaderInstance.onload = () => {
      //        let base64data = fileReaderInstance.result;
      //        setimgSrcs(imgSrcs => [...imgSrcs, base64data])
       //   }
          //console.log(base64data);
   // })


  return (
      <GridContainer>
        <GridItem xs={12}>
          <Card>
            <CardHeader color="rose" icon>
              <CardIcon color="rose">
                <Assignment />
              </CardIcon>
              <h4 className={classes.cardIconTitle}>Search Results</h4>
            </CardHeader>
            <CardBody>
              <Table
                  tableHead={props.tableData[0]}
                  tableData={renderTableData()}
                  customCellClasses={[classes.center, classes.right, classes.right]}
                  customClassesForCells={[0, 4, 5]}
                  customHeadCellClasses={[
                    classes.center,
                    classes.right,
                    classes.right
                  ]}
                  customHeadClassesForCells={[0, 4, 5]}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
  );
}
