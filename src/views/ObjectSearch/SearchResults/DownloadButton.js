import React from 'react';

import "views/UI/Modal/Button/button.scss"
import axios from "axios";

export default function DownloadButton(objectId)
{

    const onClick = () => {

        axios({
            url: 'http://localhost:8080/getobject?objectid=' + objectId.objectId,
            method: 'GET',
            responseType: 'blob',
        }).then((response) => {
            const fileName = response.headers['content-disposition'].split('filename=')[1];
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', fileName.slice(1, fileName.length - 1));
            document.body.appendChild(link);
            link.click();
        });
    }

    return(<div>
        <button className="Button" onClick={onClick}>
    Download
    </button>
    </div>)
};
