import React from "react";
// @material-ui/core components
import Table from "views/ObjectSearch/SearchResults/Table.js";

export default function SearchResults(props) {

    return (
        (props.tableData === "") ? null :
            <Table tableData={props.tableData} />
    )
}
