import React, {useCallback} from "react"
import GridContainer from "../../components/Grid/GridContainer";
import GridItem from "../../components/Grid/GridItem";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";
import Checkbox from '@material-ui/core/Checkbox';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import SearchResults from "./SearchResults/SearchResults";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../assets/jss/material-dashboard-pro-react/views/validationFormsStyle";

import ExactSearchForm from "views/ObjectSearch/Forms/ExactSearchForm.js";
import LikeSearchForm from "./Forms/LikeSearchForm";

import "./SearchResults/CenterDiv.scss"


export default function Search(){

    const [simple, setsimple] = React.useState(true);
    const [advanced, setadvanced] = React.useState(false);
    const [formdata, setformdata] = React.useState("");
    const useStyles = makeStyles(styles);

    const onSimpleChange = (event) => {
        if (!simple) {
            setsimple(event.target.checked);
            setadvanced(!event.target.checked);
        }

    }

    const onAdvancedChange = (event) => {
        if (!advanced) {
            setadvanced(event.target.checked);
            setsimple(!event.target.checked);
        }
    }

    const renderForm = () => {
        if (simple) {
            return (<LikeSearchForm parentStateSetter={wrapperSetFormData}/>)
        } else {
            return (<ExactSearchForm parentStateSetter={wrapperSetFormData}/>)
        }
    }

    const wrapperSetFormData = useCallback(val => {
        setformdata(val);
    }, [setformdata]);


    const classes = useStyles();
    return (
        <div >
            <GridContainer justify="center">
                <GridItem xs={12} sm={12} md={6}>
                    <Card>
                        <h4 className={classes.cardIconTitle}>Search For An Object</h4>
                        <CardBody>
                            <div className={"CenterDiv"}>
                            <Checkbox
                                icon={<CircleUnchecked />}
                                checkedIcon={<CircleCheckedFilled />}
                                checked={simple}
                                onChange={onSimpleChange}
                            />
                            <p className={"Label"}> Simple Search</p>
                            <Checkbox
                                icon={<CircleUnchecked />}
                                checkedIcon={<CircleCheckedFilled />}
                                checked={advanced}
                                onChange={onAdvancedChange}
                            />
                            <p className={"Label"}> Advanced Search </p>
                            </div>
                            {renderForm()}
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
            <SearchResults tableData={formdata}/>
        </div>)
}