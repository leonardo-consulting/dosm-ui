import React, { useState } from 'react'

export const GlobalAPPIDContext = React.createContext({
    globalAPPID: "",
    setGlobalAPPIDContext: () => {}
})

export const GlobalAPPIDContextProvider = (props) => {

    const setGlobalAPPIDContext = (value) => {
        setState({...state, globalAPPID: value})
    }

    const initState = {
        globalAPPID: "",
        setGlobalAPPIDContext: setGlobalAPPIDContext
    }

    const [state, setState] = useState(initState)

    return (
        <GlobalAPPIDContext.Provider value={state}>
            {props.children}
        </GlobalAPPIDContext.Provider>
    )
}